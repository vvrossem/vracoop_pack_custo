# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields, api


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.model
    def create(self, vals):
        invoice = super(AccountInvoice, self).create(vals)
        print("---- invoice -----", str(invoice))
        print("---- invoice line -----", str(invoice.invoice_line_ids))
        return invoice


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.one
    def _compute_website_order_line(self):
        self.website_order_line = self.order_line.filtered(
            lambda t: not t.pack_parent_line_id.exists(),
        )

    @api.multi
    def action_invoice_create(self, grouped=False, final=False):
        res = super(SaleOrder, self).action_invoice_create(grouped, final)
        print("------ res ------", str(res))
        print("------ grouped ------", str(grouped))
        print("------ final ------", str(final))
        return res
#         """
#         Create the invoice associated to the SO.
#         :param grouped: if True, invoices are grouped by SO id. If False, invoices are grouped by
#                         (partner_invoice_id, currency)
#         :param final: if True, refunds will be generated if necessary
#         :returns: list of created invoices
#         """
#         res = super(SaleOrder, self).action_invoice_create(grouped, final)
#         inv_obj = self.env['account.invoice']
#         precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
#         invoices = {}
#         references = {}
#         invoices_origin = {}
#         invoices_name = {}

#         for order in self:
#             group_key = order.id if grouped else (order.partner_invoice_id.id, order.currency_id.id)

#             # We only want to create sections that have at least one invoiceable line
#             pending_section = None

#             for line in order.order_line:
#                 print("---line---", str(line))
#                 if line.display_type == 'line_section':
#                     pending_section = line
#                     continue
#                 if group_key not in invoices:
#                     inv_data = order._prepare_invoice()
#                     invoice = inv_obj.create(inv_data)
#                     references[invoice] = order
#                     invoices[group_key] = invoice
#                     invoices_origin[group_key] = [invoice.origin]
#                     invoices_name[group_key] = [invoice.name]
#                 elif group_key in invoices:
#                     if order.name not in invoices_origin[group_key]:
#                         invoices_origin[group_key].append(order.name)
#                     if order.client_order_ref and order.client_order_ref not in invoices_name[group_key]:
#                         invoices_name[group_key].append(order.client_order_ref)

#                 if line.qty_to_invoice > 0 or (line.qty_to_invoice < 0 and final):
#                     print("---invoice lune create---", str(line))
#                     if pending_section:
#                         pending_section.invoice_line_create(invoices[group_key].id, pending_section.qty_to_invoice)
#                         pending_section = None
#                     line.invoice_line_create(invoices[group_key].id, line.qty_to_invoice)

#             if references.get(invoices.get(group_key)):
#                 if order not in references[invoices[group_key]]:
#                     references[invoices[group_key]] |= order

#         for group_key in invoices:
#             invoices[group_key].write({'name': ', '.join(invoices_name[group_key]),
#                                        'origin': ', '.join(invoices_origin[group_key])})
#             sale_orders = references[invoices[group_key]]
#             if len(sale_orders) == 1:
#                 invoices[group_key].reference = sale_orders.reference

#         # if not invoices:
#         #     raise UserError(_('There is no invoiceable line. If a product has a Delivered quantities invoicing policy, please make sure that a quantity has been delivered.'))

#         for invoice in invoices.values():
#             print("--- invoice ---", str(invoice))
#             invoice.compute_taxes()
#             if not invoice.invoice_line_ids:
#                 raise UserError(_('There is no invoiceable line. If a product has a Delivered quantities invoicing policy, please make sure that a quantity has been delivered.'))
#             # If invoice is negative, do a refund invoice instead
#             if invoice.amount_total < 0:
#                 invoice.type = 'out_refund'
#                 for line in invoice.invoice_line_ids:
#                     line.quantity = -line.quantity
#             # Use additional field helper function (for account extensions)
#             for line in invoice.invoice_line_ids:
#                 line._set_additional_fields(invoice)
#             # Necessary to force computation of taxes. In account_invoice, they are triggered
#             # by onchanges, which are not triggered when doing a create.
#             invoice.compute_taxes()
#             # Idem for partner
#             so_payment_term_id = invoice.payment_term_id.id
#             invoice._onchange_partner_id()
#             # To keep the payment terms set on the SO
#             invoice.payment_term_id = so_payment_term_id
#             invoice.message_post_with_view('mail.message_origin_link',
#                 values={'self': invoice, 'origin': references[invoice]},
#                 subtype_id=self.env.ref('mail.mt_note').id)
#         return [inv.id for inv in invoices.values()]


class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.multi
    def invoice_line_create(self, invoice_id, qty):
        print("---- TEST sans rien----")
        print("---- TEST ----", str(invoice_id))
        invoice_lines = super(SaleOrderLine, self).invoice_line_create(
            invoice_id, qty)
        print("---- invoice_lines ----", str(invoice_lines))
        return invoice_lines
        # invoice_lines = self.env['account.invoice.line']
        # for line in self:
        #     # Check if line has a pack_parent_line_id
        #     print("---- self.pack_parent_line_id.exists() -----", str(self.pack_parent_line_id.exists()))
        #     if self.pack_parent_line_id.exists():
        #         # Get the ref of parent pack
        #         sale_id_ref = self.env['sale.order.line'].search([('id', '=', self.pack_parent_line_id.id)])
        #         print("--- sale_id_ref ---", str(sale_id_ref))
        #         print("--- sale_id_ref.product_id.product_tmpl_id.pack_type ---", str(sale_id_ref.product_id.product_tmpl_id.pack_type))
        #         print("--- sale_id_ref.product_id.product_tmpl_id.pack_component_price ---", str(sale_id_ref.product_id.product_tmpl_id.pack_component_price))
        #         # Check if product is Pack detailed and option Totalized
        #         if sale_id_ref.product_id.product_tmpl_id.pack_type == 'detailed' and sale_id_ref.product_id.product_tmpl_id.pack_component_price == 'totalized':
        #             return invoice_lines
        # invoice_lines = super(SaleOrderLine, self).invoice_line_create(
        #     invoice_id, qty)
        # return invoice_lines