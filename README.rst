.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=================================
VRACOOP - Customisation Pack
=================================

Modifie le module Pack:
    - ajout des droits pour le website sur le modèle product.pack.line
    - cache les lignes supplémentaires des produits liés au Pack sur le Website

Credits
=======

Contributors
------------

* Juliana Poudou <juliana@le-filament.com>

Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
